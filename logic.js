$(document).ready(function(){

    
    var filterList = {
            idItems:[
                '#model',
                '#year',
                '#elevationsystem'
            ],
            nameItems:[
                'model',
                'year',
                'elevationsystem'  
            ]
        }

    var result = [];
    var errorVal = false;
    var finalResult;
    var alreadyOpen = false;
    var phrase1 = 'Your trailer is compatible with the product 1.';
    var phrase2 = 'Your trailer is compatible with the product 2.';
    var phrase3 = 'Your trailer is not compatible with our products.';
    var phrase4 = 'We havn\'t found your trailer model.';

    

    // :::::::::::::FUNCTIONS::::::::::::: //

    // ANIMATION RESULT TOGGLE
    $.fn.animationToggle = function(action, toggleForm) {

        if(action == 'close') {
            $(this).animate({height:0}, 1000, 'easeInOutExpo');
        } else {
            $(this).animate({height:290}, 1000, 'easeInOutExpo');
        }

    }

    // ANIMATION FORM TOGGLE
    var toggleForm = function() {

        $('.wrap-select').slideUp(function(){
        
            for(i = 1; i < filterList.nameItems.length; i ++) {
                $(filterList.idItems[i]).hide();
            }
            $('.button-group').hide();
            $('.again').width(0);
        });

        resetVar();
    }

    // RESET VARIABLE AND STUFF
    var resetVar = function() {

        result = [];
        alreadyOpen = false;
        $('select option').attr('selected', function() {
            return this.defaultSelected;
        });
        $('.wrap-select').slideDown();
    }


    var resultsList = function(selector, phrase) {
        $(selector).text(phrase);
    }

    var formIsValidated = function() {
        for(i = 0; i < filterList.nameItems.length; i ++) {
            
            if($(filterList.idItems[i]).val() == '0') {
                return false;
            }

        }
        return true;
    }

    $.fn.validationError = function(action) {
        
        var error = $('.message').html('<p class="error">Select all the options</p>');
        
        if(action == 'slideOn') {

            $(this).slideDown();
            errorVal = true;

        } else {

            $(this).slideUp(function(){
                $('p.error').remove();           
            });

        }
    
    }

    // :::::::::::::END FUNCTIONS::::::::::::: //



    // :::::::::::::INITIALIZATION::::::::::::: //

    $.fn.filter = function() {

        // access to JSON file to pront the values within the select boxes
    	$.getJSON('list.json', function(result){

            var dataList = [result.model, result.year, result.elevationSystem];

            for(i = 0; i < filterList.nameItems.length; i ++) {
                var n = 1;
                // load the values within select boxes
    			$.each(dataList[i], function(index, field){ 
    				$(filterList.idItems[i]).append('<option value="' + n + '">' + field + '</option>');
                    n++;
    			});
            }

    	});

    }



    // :::::::::::::WHEN SELECT FROM THE COMBO BOXES::::::::::::: //

    $('select').change(function(){

        var itemSelected = $(this).find(':selected').text();
        var selector = $(this).attr('id');

        for(i = 0; i < filterList.nameItems.length; i ++) {

            var filterListData = filterList.nameItems[i] + 'Data';

            switch(selector) {

                case filterList.nameItems[i]:
                    result[filterListData] = itemSelected;
                    $(this).next().slideDown('swing');
                    break;
            }
        }
    });



    // :::::::::::::PARSE THE RESULTS::::::::::::: //

    $('#submit').click(function(e){

        e.preventDefault();

        if( formIsValidated() ) {

            if(errorVal){
                $('.message').validationError('slideOff');
            }

            $.getJSON('combo.json', function(check) {

                finalResult = [];
                var checkLength = Object.keys(check).length;

                // loop through the combo json product 1, product 2 and uncompatible to match the picked values
                for(i = 0; i < checkLength; i++) {

                    $.each(check, function(index, field) {
                       if(result['modelData'] == field[i].model && result['yearData'] == field[i].year && result['elevationsystemData'] == field[i].elevationSystem) {
                            finalResult = index;
                            return finalResult;
                       }

                    });
                }

                if(alreadyOpen) {

                    $('.outer-wrap').animationToggle('close', toggleForm);

                } else (

                    alreadyOpen = true

                )


                switch(finalResult) {

                    // Parse the results
                    case 'product1':

                        resultsList('#great p', phrase1);
                        $('#great').animationToggle('open');
                        break;

                    case 'product2':

                        resultsList('#great p', phrase2);
                        $('#great').animationToggle('open');
                        break;

                    case 'incompatible':

                        resultsList('#sorry p', phrase3);
                        $('#sorry').animationToggle('open');
                        break;

                    default:

                        resultsList('#sorry p', phrase4);
                        $('#sorry').animationToggle('open');

                }

                $('.again').animate({width:123});

            });

        } else {

            $('.message').validationError('slideOn');

        }

    });


    // :::::::::::::RESTART IT::::::::::::: //
    $('.again').click(function(){   

        $('.outer-wrap').animationToggle('close', {

            callback: toggleForm(),
            closeError: $('.message').validationError('slideOff')                  

        });
        
    });


});

